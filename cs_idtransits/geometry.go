package main

import "errors"

type Vector [2]float64

type PointXY struct {
	x, y float64
}

type VectorXY struct {
	a, b PointXY
}

func crossProd2(u, v Vector) float64 {
	return u[0]*v[1] - u[1]*v[0]
}

// contains tests whether a point is within a bbox
func contains(xmin, ymin, xmax, ymax float64, pt PointXY) bool {
	if (xmin < pt.x) && (pt.x < xmax) && (ymin < pt.y) && (pt.y < ymax) {
		return true
	} else {
		return false
	}
}

// intersection computes where two cartesian vectors intersect
// returns a non-nil error if they don't
func intersection(vec1, vec2 *VectorXY) (*PointXY, error) {
	r := Vector{vec1.b.x - vec1.a.x, vec1.b.y - vec1.a.y}
	s := Vector{vec2.b.x - vec2.a.x, vec2.b.y - vec2.a.y}
	rxs := crossProd2(r, s)

	if rxs == 0 {
		// Lines are parallel or collinear (treat as non-intersecting)
		return nil, errors.New("collinear or parallel vectors")
	}

	p := vec1.a
	q := vec2.a
	t := crossProd2(Vector{q.x - p.x, q.y - p.y}, s) / rxs
	u := crossProd2(Vector{q.x - p.x, q.y - p.y}, r) / rxs

	// use t in (0,1], u in (0,1] to avoid problems when point is exactly on box edge
	if (1e-12 <= t) && (t <= 1) && (1e-12 <= u) && (u <= 1) {
		return &PointXY{p.x + t*r[0], p.y + t*r[1]}, nil
	} else {
		return nil, errors.New("non-intersecting")
	}
}
