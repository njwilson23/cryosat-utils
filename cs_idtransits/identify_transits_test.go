package main

import (
	"fmt"
	"testing"
)

func TestContains(t *testing.T) {
	var tf bool
	tf = contains(-10, -10, 5, 5, PointXY{-2, 1})
	if !tf {
		t.Fail()
	}

	tf = contains(-10, -10, 5, 5, PointXY{-12, 1})
	if tf {
		t.Fail()
	}
}

func TestCrossProd2(t *testing.T) {
	var out float64
	out = crossProd2(Vector{0, 1}, Vector{0, 1})
	if out != 0.0 {
		t.Fail()
	}

	out = crossProd2(Vector{1, 0}, Vector{0, 1})
	if out != 1.0 {
		t.Fail()
	}
}

func TestIntersectionParallel(t *testing.T) {
	var err error

	_, err = intersection(
		&VectorXY{PointXY{0, 0}, PointXY{1, 1}},
		&VectorXY{PointXY{1, 0.5}, PointXY{2, 1.5}})
	if err == nil {
		fmt.Println("non-intersecting parallel lines not detected")
		t.Fail()
	}
}

func TestIntersectionNon(t *testing.T) {
	var err error

	_, err = intersection(
		&VectorXY{PointXY{0, 0}, PointXY{1, 1}},
		&VectorXY{PointXY{2, -1}, PointXY{1, 0}})
	if err == nil {
		fmt.Println("non-intersecting non-parallel lines not detected")
		t.Fail()
	}
}

func TestIntersectionCollinear(t *testing.T) {
	var err error

	_, err = intersection(
		&VectorXY{PointXY{0, 0}, PointXY{1, 1}},
		&VectorXY{PointXY{0.5, 0.9}, PointXY{0.5, 0.9}})
	if err == nil {
		fmt.Println("collinear lines not detected")
		t.Fail()
	}
}

func TestIntersection(t *testing.T) {
	var interx *PointXY
	var err error

	interx, err = intersection(
		&VectorXY{PointXY{0, 0}, PointXY{1, 1}},
		&VectorXY{PointXY{1, 0}, PointXY{0, 1}})
	if (err != nil) || (interx.x != 0.5) || (interx.y != 0.5) {
		fmt.Println("intersection not computed")
		t.Fail()
	}
}

func TestParseDateString(t *testing.T) {
	s := "2010-06-15_00:03:00.100000"
	timestamp, err := parseDateString(s)
	if err != nil {
		fmt.Println(err)
		t.Error()
	}
	if timestamp != 1276560180 {
		t.Fail()
	}
	/* verify with
	import datetime
	d = datetime.datetime(2010, 6, 15, 0, 3, 0, 100, tzinfo=datetime.timezone.utc)
	print(int(d.timestamp()))
	*/
}

func TestFmtTime(t *testing.T) {
	s := fmtTime(1276560180)
	if s != "2010-06-15_00:03:00.000000" {
		t.Fail()
	}
}

func TestTimeInterp(t *testing.T) {
	ts := timeInterp(
		&Record{1000, 1, 50, 32},
		&Record{1200, 1, 54, 33},
		&PointXY{53, 32.75})
	if ts != 1150 {
		t.Fail()
	}
}

func TestTimeInterp_Extrap(t *testing.T) {
	ts := timeInterp(
		&Record{1000, 1, 50, 32},
		&Record{1200, 1, 54, 33},
		&PointXY{48, 31.5})
	if ts != 900 {
		t.Fail()
	}
}
