package main

import (
	"bufio"
	"flag"
	"fmt"
	"io"
	"math"
	"os"
	"sort"
	"strconv"
	"time"
)

func usage() string {
	return fmt.Sprintf("%s",
		"cs_idtransits -xmin XMIN -ymin YMIN -xmax XMAX -ymax YMAX INPUT OUTPUT")
}

type Record struct {
	dateInt   int64
	orbitNum  int
	longitude float64
	latitude  float64
}

// roundPositive rounds an a positive number to the nearest integer
func roundPositive(a float64) int64 {
	return int64(a + 0.5)
}

// parseDateString converts a string representing UTC date into an integer of
// seconds since January 1, 1970
//
// e.g. 2010-06-15_00:00:00.100000
func parseDateString(dateString string) (int64, error) {
	example := "2006-01-02_15:04:05.000000"
	t, err := time.Parse(example, dateString)
	if err != nil {
		panic(err)
	}
	return t.Unix(), nil
}

// fmtTime returns a string representing a unix timestamp as a UTC time
func fmtTime(timeInt int64) string {
	t := time.Unix(timeInt, int64(0)).UTC()
	return t.Format("2006-01-02_15:04:05.000000")
}

// ReadTable reads successive non-comment rows from a text table, emmitting
// them into a channel
func readTable(f *os.File, records chan<- Record) {
	var dateString, orbitString, lonString, latString string
	var orbitInt, dateInt int64
	var longitude, latitude float64
	var record Record
	var err error
	var n int
	var line string

	reader := bufio.NewReader(f)

	for {
		line, err = reader.ReadString('\n')
		if err == io.EOF {
			break
		} else if err != nil {
			panic(err)
		}

		if line[0] == '#' {
			continue
		}

		n, err = fmt.Sscan(line, &dateString, &orbitString, &lonString, &latString)
		if err != nil {
			if n == 0 {
				break
			} else {
				panic(err)
			}
		}

		dateInt, err = parseDateString(dateString)
		if err != nil {
			panic(err)
		}

		orbitInt, err = strconv.ParseInt(orbitString, 10, 32)
		if err != nil {
			panic(err)
		}

		longitude, err = strconv.ParseFloat(lonString, 64)
		if err != nil {
			panic(err)
		}
		longitude = math.Mod(longitude+180, 360.0) - 180.0

		latitude, err = strconv.ParseFloat(latString, 64)
		if err != nil {
			panic(err)
		}

		record = Record{dateInt, int(orbitInt), longitude, latitude}
		records <- record
	}
	close(records)
}

func timeInterp(rec0, rec1 *Record, pt *PointXY) int64 {
	var f float64
	if rec0.longitude != rec1.longitude {
		f = (pt.x - rec0.longitude) / (rec1.longitude - rec0.longitude)
		return roundPositive(float64(rec0.dateInt) + f*float64(rec1.dateInt-rec0.dateInt))
	} else if rec0.latitude != rec1.latitude {
		f = (pt.y - rec0.latitude) / (rec1.latitude - rec0.latitude)
		return roundPositive(float64(rec0.dateInt) + f*float64(rec1.dateInt-rec0.dateInt))
	} else {
		fmt.Println("warning: co-located points")
		return roundPositive(0.5 * float64(rec0.dateInt+rec1.dateInt))
	}
}

// computeCrossingTimes compares two records, and counts the times that a flight between
// the two would cross the edge of a bounding box. If the number of crossings is not zero,
// the second return value is the interpolated timestamp of the crossing
func computeCrossingTimes(xmin, ymin, xmax, ymax float64, rec0, rec1 Record) (int, []int64) {
	var xLeft, xRight, xBottom, xTop *PointXY
	var err error
	var cnt int = 0

	flight := &VectorXY{
		PointXY{rec0.longitude, rec0.latitude},
		PointXY{rec1.longitude, rec1.latitude}}

	times := make([]int64, 2, 2)

	xLeft, err = intersection(&VectorXY{PointXY{xmin, ymin}, PointXY{xmin, ymax}}, flight)
	if err == nil {
		times[cnt] = timeInterp(&rec0, &rec1, xLeft)
		cnt++
	}

	xRight, err = intersection(&VectorXY{PointXY{xmax, ymin}, PointXY{xmax, ymax}}, flight)
	if err == nil {
		times[cnt] = timeInterp(&rec0, &rec1, xRight)
		cnt++
	}
	if cnt == 2 {
		return 2, times
	}

	xBottom, err = intersection(&VectorXY{PointXY{xmin, ymin}, PointXY{xmax, ymin}}, flight)
	if err == nil {
		times[cnt] = timeInterp(&rec0, &rec1, xBottom)
		cnt++
	}
	if cnt == 2 {
		return 2, times
	}

	xTop, err = intersection(&VectorXY{PointXY{xmin, ymax}, PointXY{xmax, ymax}}, flight)
	if err == nil {
		times[cnt] = timeInterp(&rec0, &rec1, xTop)
		cnt++
	}
	return cnt, times
}

type AscendingInt64 []int64

func (a AscendingInt64) Len() int           { return len(a) }
func (a AscendingInt64) Swap(i, j int)      { a[i], a[j] = a[j], a[i] }
func (a AscendingInt64) Less(i, j int) bool { return a[i] < a[j] }

// findTransits checks whether record enter, exit, or cross the target
// bounding box, and writes the interpolated times to a file
func findTransits(xmin, ymin, xmax, ymax float64, records <-chan Record, done chan<- bool, fout *os.File) {
	var record, prevRecord Record
	prevRecord = <-records

	var ISINSIDE bool
	if contains(xmin, ymin, xmax, ymax, PointXY{prevRecord.longitude, prevRecord.latitude}) {
		ISINSIDE = true
	} else {
		ISINSIDE = false
	}

	var err error
	var n, counter, nCrossings int = 0, 0, 0
	var times []int64

	for record = range records {
		n, times = computeCrossingTimes(xmin, ymin, xmax, ymax, prevRecord, record)
		if n == 1 {
			if ISINSIDE {
				nCrossings++
				_, err = fout.Write([]byte(fmt.Sprintf("%s\n", fmtTime(times[0]))))
				if err != nil {
					panic(err)
				}
			} else {
				_, err = fout.Write([]byte(fmt.Sprintf("%s\t", fmtTime(times[0]))))
				if err != nil {
					panic(err)
				}
			}
			ISINSIDE = !(ISINSIDE)
		} else if n == 2 {
			sort.Sort(AscendingInt64(times))
			_, err = fout.Write([]byte(fmt.Sprintf("%s\t%s\n", fmtTime(times[0]), fmtTime(times[1]))))
			if err != nil {
				panic(err)
			}
			nCrossings++
		}
		prevRecord = record

		counter++
		if math.Mod(float64(counter), 100000.0) == 0 {
			fmt.Printf("Processed %d records and found %d transits\n", counter, nCrossings)
		}
	}
	fmt.Printf("%s %d %s\n", "found", nCrossings, "transits")
	done <- true
}

func main() {
	xmin := flag.Float64("xmin", -180, "Western boundary")
	ymin := flag.Float64("ymin", -90, "Southern boundary")
	xmax := flag.Float64("xmax", 180, "Eastern boundary")
	ymax := flag.Float64("ymax", 90, "Northern boundary")
	flag.Parse()

	if len(flag.Args()) != 2 {
		fmt.Println("Missing required arguments. Use as:")
		fmt.Println("\t", usage())
		os.Exit(1)
	}
	infile := flag.Arg(0)
	outfile := flag.Arg(1)

	fin, err := os.Open(infile)
	if err != nil {
		panic(err)
	}
	defer fin.Close()

	fout, err := os.Create(outfile)
	if err != nil {
		panic(err)
	}
	defer fout.Close()

	records := make(chan Record, 2)
	done := make(chan bool) // used to signal completion

	go readTable(fin, records)
	go findTransits(*xmin, *ymin, *xmax, *ymax, records, done, fout)

	// block until done
	<-done
}
