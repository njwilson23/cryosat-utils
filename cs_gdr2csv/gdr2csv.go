package main

import (
	"flag"
	"fmt"
	"math"
	"os"
	"strconv"
)

func main() {

	var xmin, ymin, xmax, ymax float64
	flag.Float64Var(&xmin, "xmin", -180, "Minimum allowable longitude")
	flag.Float64Var(&xmax, "xmax", 180, "Maximum allowable longitude")
	flag.Float64Var(&ymin, "ymin", -90, "Minimum allowable latitude")
	flag.Float64Var(&ymax, "ymax", 90, "Maximum allowable latitude")
	flag.Parse()

	inputFilename := flag.Arg(0)
	outputFilename := flag.Arg(1)
	if len(inputFilename) == 0 {
		panic("input file not provided")
	}
	if len(outputFilename) == 0 {
		panic("output file not provided")
	}

	fmt.Println("ARGS:", inputFilename, outputFilename)
	fmt.Println("restricting to bounding box", xmin, ymin, xmax, ymax)

	filePtr, err := os.Open(inputFilename)
	if err != nil {
		panic(err)
	}
	defer filePtr.Close()

	mainHeader, err := ParseMPH(filePtr)
	if err != nil {
		panic("failure parsing main header")
	}
	/*specificHeader, err := ParseSPH(filePtr)
	if err != nil {
		panic("failure parsing specific header")
	}*/

	numDSDDescriptors, err := strconv.Atoi(mainHeader["NUM_DSD"])
	datasetDescriptors := make([]*DataSetDescriptor, numDSDDescriptors)
	var dsd *DataSetDescriptor
	var i int
	for i = 0; i != numDSDDescriptors; i++ {
		dsd, err = ParseDSD(filePtr, i)
		datasetDescriptors[i] = dsd
	}

	data, err := ParseMDS(filePtr, datasetDescriptors[0])
	if err != nil {
		panic("failure parsing data")
	}

	if datasetDescriptors[0].NumRecords != len(data) {
		panic("number of records read does not match number expected")
	} else {
		fmt.Println("number of records:", datasetDescriptors[0].NumRecords)
	}

	// write output
	outPtr, err := os.Create(outputFilename)
	if err != nil {
		panic(err)
	}
	defer outPtr.Close()

	var dataBounds struct{ xmin, ymin, xmax, ymax float64 }

	var timeDays, timeSeconds, timeMicroseconds int
	var deltaTimeMicroseconds int
	var lat, lon, elev float64
	var quality, qualFlag int
	var record *Record
	var j int
	count := 0
	for i = 0; i != len(data); i++ {
		record = data[i]
		timeDays, timeSeconds, timeMicroseconds = record.Time()
		for j = 0; j != record.NValidMeasurements(); j++ {
			lat, lon = record.Measurements[j].Coordinates()

			dataBounds.xmin = math.Min(dataBounds.xmin, lon)
			dataBounds.xmax = math.Max(dataBounds.xmax, lon)
			dataBounds.ymin = math.Min(dataBounds.ymin, lat)
			dataBounds.ymax = math.Max(dataBounds.ymax, lat)

			if (xmin <= lon) && (xmax >= lon) && (ymin <= lat) && (ymax >= lat) {
				deltaTimeMicroseconds = record.Measurements[j].DeltaTime()
				elev = record.Measurements[j].SurfaceHeight_m()
				quality = record.Measurements[j].HeightQualityChi2()
				qualFlag = record.Measurements[j].QualityFlag()
				fmt.Fprintf(outPtr, "%d, %d, %d, ", timeDays, timeSeconds, timeMicroseconds+deltaTimeMicroseconds)
				fmt.Fprintf(outPtr, "%.2f, %.2f, %.2f, %d, %d\n", lon, lat, elev, quality, qualFlag)
				count++
			}
		}
	}
	fmt.Println("found", count, "suitable records")
	fmt.Println("data bounds", dataBounds)
}
