/* product.go
 *
 * functions for reading CryoSat product file (*.DBL)
 */

package main

import (
	"bytes"
	"encoding/binary"
	"errors"
	"fmt"
	"os"
	"strconv"
	"strings"
)

type LocationGroup struct {
	time       [12]byte
	mode       [8]byte
	latitude   [4]byte
	longitude  [4]byte
	altitude   [4]byte
	rollAngle  [4]byte
	pitchAngle [4]byte
	yawAngle   [4]byte
	spare      [2]byte
	nValid     [2]byte
}

type ExternalCorrectionsGroup struct {
	dryTropospheric       [2]byte
	wetTropospheric       [2]byte
	inverseBarometric     [2]byte
	dynamicAtmospheric    [2]byte
	ionospheric           [2]byte
	seaStateBias          [2]byte
	elasticOceanTide      [2]byte
	oceanLoadingTide      [2]byte
	solidEarthTide        [2]byte
	geocentricPolarTide   [2]byte
	spare                 [2]byte
	surfaceType           [8]byte
	geoid                 [4]byte
	depthOrElevation      [4]byte
	iceConcentration      [2]byte
	snowDepth             [2]byte
	snowDensity           [2]byte
	spare2                [2]byte
	correctionsStatus     [4]byte
	significantWaveHeight [2]byte
	windSpeed             [2]byte
	spare3                [8]byte
}

type MeasurementsGroup struct {
	deltaTime               [4]byte
	latitude                [4]byte
	longitude               [4]byte
	height1                 [4]byte
	height2                 [4]byte
	height3                 [4]byte
	backscatter1            [2]byte
	backscatter2            [2]byte
	backscatter3            [2]byte
	freeboard               [2]byte
	seaSurfaceHeightAnomaly [2]byte
	nInterpRecords          [2]byte
	sSHAQuality             [2]byte
	peakiness               [2]byte
	nEchosAveraged          [2]byte
	spare                   [2]byte
	quality                 [4]byte
	correctionApplication   [4]byte
	retrackerQuality1       [4]byte
	retrackerQuality2       [4]byte
	retrackerQuality3       [4]byte
}

type DataSetDescriptor struct {
	Name, Type, Filename                 string
	Offset, Size, NumRecords, RecordSize int
}

type Record struct {
	Location     LocationGroup
	Corrections  ExternalCorrectionsGroup
	Measurements [20]MeasurementsGroup
}

// ParseMPH reads the main product header from the CrySat data file and returns
// a string to string map of the fields and values
func ParseMPH(f *os.File) (map[string]string, error) {
	f.Seek(0, 0)
	bytestr := make([]byte, 1247)
	_, err := f.Read(bytestr)
	if err != nil {
		panic(err)
	}

	mphDict := make(map[string]string)
	buffer := make([]byte, 100)
	pos := 0
	bufferCnt := 0
	var key, value string
	for pos != 1247 {
		buffer[bufferCnt] = bytestr[pos]
		if string(buffer[bufferCnt]) == "\n" {
			key, value, err = parseKeyValue(string(buffer[:bufferCnt]))
			if err != nil {
				panic(err)
			} else {
				mphDict[key] = value
			}
			bufferCnt = 0
		} else {
			bufferCnt++
		}
		pos++
	}
	return mphDict, nil
}

// ParseSPH reads the spcific product file header from an open file.
func ParseSPH(f *os.File) (map[string]string, error) {
	f.Seek(1247, 0)
	bytestr := make([]byte, 1227)
	_, err := f.Read(bytestr)
	if err != nil {
		panic(err)
	}

	sphDict := make(map[string]string)
	buffer := make([]byte, 100)

	pos := 0
	bufferCnt := 0
	var key, value string
	for pos != 1227 {
		buffer[bufferCnt] = bytestr[pos]
		if string(buffer[bufferCnt]) == "\n" {
			key, value, err = parseKeyValue(string(buffer[:bufferCnt]))
			if err != nil {
				panic(err)
			} else {
				sphDict[key] = value
			}
			bufferCnt = 0
		} else {
			bufferCnt++
		}
		pos++
	}
	return sphDict, nil
}

// ParseDSD reads the (DSDNum)th dataset descriptor (DSD) from an os.File
// pointer and returns a pointer to a DataSetDescriptor struct
func ParseDSD(f *os.File, DSDNum int) (*DataSetDescriptor, error) {
	bytestr := make([]byte, 280)
	start := int64(1247 + 1227 + DSDNum*280)
	f.Seek(start, 0)
	_, err := f.Read(bytestr)
	if err != nil {
		panic(err)
	}

	var dsd DataSetDescriptor
	dsd.Name = string(bytestr[9:37])
	dsd.Type = string(bytestr[47])
	dsd.Filename = string(bytestr[59:121])
	dsd.Offset, err = strconv.Atoi(string(bytestr[133:154]))
	if err != nil {
		panic(err)
	}
	dsd.Size, err = strconv.Atoi(string(bytestr[170:191]))
	if err != nil {
		panic(err)
	}
	dsd.NumRecords, err = strconv.Atoi(string(bytestr[207:218]))
	if err != nil {
		panic(err)
	}
	dsd.RecordSize, err = strconv.Atoi(string(bytestr[228:239]))
	if err != nil {
		panic(err)
	}
	return &dsd, err
}

// ParseMDS reads the data block from a CryoSat data file
func ParseMDS(f *os.File, dataset *DataSetDescriptor) ([]*Record, error) {
	// allocate record array and byte slice
	records := make([]*Record, dataset.NumRecords)
	bytestr := make([]byte, dataset.Size)

	// read in data
	f.Seek(int64(dataset.Offset), 0)
	_, err := f.Read(bytestr)
	if err != nil {
		panic(err)
	}

	// read each record
	var record *Record
	for i := 0; i != dataset.NumRecords; i++ {
		record, err = readRecord(bytestr[i*1392 : (i+1)*1392])
		if err != nil {
			panic(err)
		}
		records[i] = record
		//i = dataset.NumRecords - 1 // debug
	}
	return records, nil
}

// parseKeyValue splits a string containing one equals sign (=) and returns the
// LHS and RHS strings
func parseKeyValue(s string) (string, string, error) {
	var key, value string
	var err error
	parts := strings.SplitN(s, "=", 2)

	key = parts[0]
	if len(parts) == 2 {
		value = parts[1]
		if strings.Contains(parts[1], "=") {
			err = errors.New("ambiguous record split")
		}
	} else {
		value = ""
	}
	return key, value, err
}

// readRecord interprets a 1392 byte sequence as a Record struct and returns a
// pointer and an error
func readRecord(bytestr []byte) (*Record, error) {
	if len(bytestr) != 1392 {
		return nil, errors.New("byte slice wrong length for record")
	}

	var location LocationGroup
	var corrections ExternalCorrectionsGroup
	var measurements [20]MeasurementsGroup

	// set location group
	copy(location.time[:], bytestr[:12])
	copy(location.mode[:], bytestr[12:20])
	copy(location.latitude[:], bytestr[20:24])
	copy(location.longitude[:], bytestr[24:28])
	copy(location.altitude[:], bytestr[28:32])
	copy(location.rollAngle[:], bytestr[32:36])
	copy(location.pitchAngle[:], bytestr[36:40])
	copy(location.yawAngle[:], bytestr[40:44])
	copy(location.spare[:], bytestr[44:46])
	copy(location.nValid[:], bytestr[46:48])

	// set external corrections group
	copy(corrections.dryTropospheric[:], bytestr[48:50])
	copy(corrections.wetTropospheric[:], bytestr[50:52])
	copy(corrections.inverseBarometric[:], bytestr[52:54])
	copy(corrections.dynamicAtmospheric[:], bytestr[54:56])
	copy(corrections.ionospheric[:], bytestr[56:58])
	copy(corrections.seaStateBias[:], bytestr[58:60])
	copy(corrections.elasticOceanTide[:], bytestr[60:62])
	copy(corrections.oceanLoadingTide[:], bytestr[62:64])
	copy(corrections.solidEarthTide[:], bytestr[64:66])
	copy(corrections.geocentricPolarTide[:], bytestr[66:68])
	copy(corrections.spare[:], bytestr[68:70])
	copy(corrections.surfaceType[:], bytestr[70:78])
	copy(corrections.geoid[:], bytestr[78:84])
	copy(corrections.depthOrElevation[:], bytestr[84:88])
	copy(corrections.iceConcentration[:], bytestr[88:90])
	copy(corrections.snowDepth[:], bytestr[90:92])
	copy(corrections.snowDensity[:], bytestr[92:94])
	copy(corrections.spare2[:], bytestr[94:96])
	copy(corrections.correctionsStatus[:], bytestr[96:100])
	copy(corrections.significantWaveHeight[:], bytestr[100:102])
	copy(corrections.windSpeed[:], bytestr[102:104])
	copy(corrections.spare3[:], bytestr[104:112])

	// set measurements
	nvalid := binary.BigEndian.Uint16(location.nValid[:])
	var subbytestr []byte
	var meas *MeasurementsGroup
	var i int
	for i = 0; i != int(nvalid); i++ {
		meas = &measurements[i]
		subbytestr = bytestr[i*64+112 : i*64+176]

		copy(meas.deltaTime[:], subbytestr[0:4])
		copy(meas.latitude[:], subbytestr[4:8])
		copy(meas.longitude[:], subbytestr[8:12])
		copy(meas.height1[:], subbytestr[12:16])
		copy(meas.height2[:], subbytestr[16:20])
		copy(meas.height3[:], subbytestr[20:24])
		copy(meas.backscatter1[:], subbytestr[24:26])
		copy(meas.backscatter2[:], subbytestr[26:28])
		copy(meas.backscatter3[:], subbytestr[28:30])
		copy(meas.freeboard[:], subbytestr[30:32])
		copy(meas.seaSurfaceHeightAnomaly[:], subbytestr[32:34])
		copy(meas.nInterpRecords[:], subbytestr[34:36])
		copy(meas.sSHAQuality[:], subbytestr[36:38])
		copy(meas.peakiness[:], subbytestr[38:40])
		copy(meas.nEchosAveraged[:], subbytestr[40:42])
		copy(meas.spare[:], subbytestr[42:44])
		copy(meas.quality[:], subbytestr[44:48])
		copy(meas.correctionApplication[:], subbytestr[48:52])
		copy(meas.retrackerQuality1[:], subbytestr[52:56])
		copy(meas.retrackerQuality2[:], subbytestr[56:60])
		copy(meas.retrackerQuality3[:], subbytestr[60:64])
	}

	record := Record{location, corrections, measurements}
	return &record, nil
}

// Time() returns time of record in days, seconds, and microseconds past 2000-01-01
func (r *Record) Time() (int, int, int) {
	var days int32
	var sec, microsec uint32
	binary.Read(bytes.NewReader(r.Location.time[:4]), binary.BigEndian, &days)
	binary.Read(bytes.NewReader(r.Location.time[4:8]), binary.BigEndian, &sec)
	binary.Read(bytes.NewReader(r.Location.time[8:12]), binary.BigEndian, &microsec)
	return int(days), int(sec), int(microsec)
}

// DeltaTime() returns timedelta in microseconds from the record time and
// measurement time
func (m *MeasurementsGroup) DeltaTime() int {
	var microsec int32
	binary.Read(bytes.NewReader(m.deltaTime[:]), binary.BigEndian, &microsec)
	return int(microsec)
}

// Coordinates returns Latitude, Longitude
func (m *MeasurementsGroup) Coordinates() (float64, float64) {
	var lat, lon int32
	binary.Read(bytes.NewReader(m.latitude[:]), binary.BigEndian, &lat)
	binary.Read(bytes.NewReader(m.longitude[:]), binary.BigEndian, &lon)
	flat := float64(lat) / 1e7
	flon := float64(lon) / 1e7
	return flat, flon
}

// SurfaceHeight_m returns surface elevation relative to WGS84 ellipsoid in meters
func (m *MeasurementsGroup) SurfaceHeight_m() float64 {
	var height_mm int32
	binary.Read(bytes.NewReader(m.height1[:]), binary.BigEndian, &height_mm)
	return float64(height_mm) / 1e3
}

// HeightQualityChi2 returns chi2 fit form retracker 1
func (m *MeasurementsGroup) HeightQualityChi2() int {
	var val int16
	binary.Read(bytes.NewReader(m.retrackerQuality1[:]), binary.BigEndian, &val)
	return int(val)
}

// NumEchosAveraged returns number of echos averaged/stacked from retracker 1
func (m *MeasurementsGroup) NumEchosAveraged() int {
	var val int32
	binary.Read(bytes.NewReader(m.nEchosAveraged[:]), binary.BigEndian, &val)
	return int(val)
}

// QualityFlag returns the bit-packed quality information from a measurement
func (m *MeasurementsGroup) QualityFlag() int {
	var val int32
	binary.Read(bytes.NewReader(m.quality[:]), binary.BigEndian, &val)
	return int(val)
}

// NValidMeasurements return the number of valid measurements in the
// Record.measurements array
func (r *Record) NValidMeasurements() int {
	var nv int16
	binary.Read(bytes.NewReader(r.Location.nValid[:]), binary.BigEndian, &nv)
	return int(nv)
}

func debug() {
	fmt.Println("")
}
